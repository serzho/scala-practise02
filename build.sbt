seq(webSettings :_*)

name := "scala-practise02"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies ++= Seq(
    "org.eclipse.jetty" % "jetty-webapp" % "8.0.1.v20110908" % "container",
    "javax.servlet" % "servlet-api" % "2.5" % "provided",
    "com.practise" %% "scala-practise01" % "1.0",
    "org.scalatest" %% "scalatest" % "1.9.1" % "test"
    )
